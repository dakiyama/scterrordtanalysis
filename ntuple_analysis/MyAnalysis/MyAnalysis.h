//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Jun  8 00:15:23 2023 by ROOT version 6.24/08
// from TChain MyTree/
//////////////////////////////////////////////////////////

#ifndef MyAnalysis_h
#define MyAnalysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

class MyAnalysis {
 public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Fixed size dimensions of array or collections stored in the TTree if any.

  // Declaration of leaf types
  Int_t           is_data;
  Int_t           runNumber;
  Int_t           lumiBlock;
  ULong64_t       eventNumber;
  Float_t         average_interaction_per_crossing;
  Float_t         actual_interaction_per_crossing;
  Int_t           has_pv;
  Int_t           pass_data_quality;
  Int_t           pass_event_cleaning;
  Int_t           nMuons;
  Int_t           nElectrons;
  Int_t           nJets;
  Int_t           nMSTracks;
  Int_t           nTracks;
  Int_t           nTruths;
  vector<float>   *muon_phi;
  vector<float>   *muon_eta;
  vector<float>   *muon_pt;
  vector<float>   *muon_e;
  vector<int>     *muon_charge;
  vector<float>   *electron_phi;
  vector<float>   *electron_eta;
  vector<float>   *electron_pt;
  vector<float>   *electron_e;
  vector<int>     *electron_charge;
  vector<float>   *jet_phi;
  vector<float>   *jet_eta;
  vector<float>   *jet_pt;
  vector<float>   *jet_e;
  vector<float>   *truth_phi;
  vector<float>   *truth_eta;
  vector<float>   *truth_pt;
  vector<float>   *truth_e;
  vector<float>   *truth_vis_phi;
  vector<float>   *truth_vis_eta;
  vector<float>   *truth_vis_pt;
  vector<float>   *truth_vis_e;
  vector<int>     *truth_charge;
  vector<int>     *truth_pdgid;
  vector<int>     *truth_barcode;
  vector<int>     *truth_nChildren;
  vector<int>     *truth_nTauProngs;
  vector<int>     *truth_nPi0;
  vector<int>     *truth_isTauHad;
  vector<int>     *truth_parent_barcode;
  vector<float>   *truth_properTime;
  vector<float>   *truth_decayr;
  vector<float>   *msTrack_phi;
  vector<float>   *msTrack_eta;
  vector<float>   *msTrack_pt;
  vector<float>   *msTrack_e;
  vector<int>     *msTrack_charge;
  vector<int>     *track_isTracklet;
  vector<float>   *track_phi;
  vector<float>   *track_eta;
  vector<float>   *track_pt;
  vector<float>   *track_e;
  vector<float>   *track_charge;
  vector<int>     *track_pixelBarrel_0;
  vector<int>     *track_pixelBarrel_1;
  vector<int>     *track_pixelBarrel_2;
  vector<int>     *track_pixelBarrel_3;
  vector<int>     *track_sctBarrel_0;
  vector<int>     *track_sctBarrel_1;
  vector<int>     *track_sctBarrel_2;
  vector<int>     *track_sctBarrel_3;
  vector<int>     *track_sctEndcap_0;
  vector<int>     *track_sctEndcap_1;
  vector<int>     *track_sctEndcap_2;
  vector<int>     *track_sctEndcap_3;
  vector<int>     *track_sctEndcap_4;
  vector<int>     *track_sctEndcap_5;
  vector<int>     *track_sctEndcap_6;
  vector<int>     *track_sctEndcap_7;
  vector<int>     *track_sctEndcap_8;
  vector<int>     *track_nPixHits;
  vector<int>     *track_nPixHoles;
  vector<int>     *track_nPixSharedHits;
  vector<int>     *track_nPixOutliers;
  vector<int>     *track_nPixDeadSensors;
  vector<int>     *track_nContribPixelLayers;
  vector<int>     *track_nGangedFlaggedFakes;
  vector<int>     *track_nPixelSpoiltHits;
  vector<int>     *track_nSCTHits;
  vector<int>     *track_nSCTHoles;
  vector<int>     *track_nSCTSharedHits;
  vector<int>     *track_nSCTOutliers;
  vector<int>     *track_nSCTDeadSensors;
  vector<int>     *track_nTRTHits;
  vector<int>     *track_nTRTHoles;
  vector<int>     *track_nTRTSharedHits;
  vector<int>     *track_nTRTDeadSensors;
  vector<int>     *track_nTRTOutliers;
  vector<float>   *track_dEdx;
  vector<float>   *track_dRJet20;
  vector<float>   *track_dRJet50;
  vector<float>   *track_dRMSTrack;
  vector<float>   *track_dRMuon;
  vector<float>   *track_dRElectron;
  vector<float>   *track_d0;
  vector<float>   *track_d0Sig;
  vector<float>   *track_z0;
  vector<float>   *track_z0PV;
  vector<float>   *track_z0PVSinTheta;
  vector<float>   *track_chi2Prob;
  std::vector<float>   *track_truth_phi;
  std::vector<float>   *track_truth_eta;
  std::vector<float>   *track_truth_pt;
  std::vector<float>   *track_truth_e;
  std::vector<int>     *track_truth_charge;
  std::vector<int>     *track_truth_pdgid;
  std::vector<int>     *track_truth_barcode;
  std::vector<int>     *track_truth_nChildren;
  std::vector<float>   *track_truth_properTime;
  std::vector<float>   *track_truth_decayr;
  std::vector<float>   *track_truth_matchingProb;
  std::vector<float>   *track_truth_parent_phi;
  std::vector<float>   *track_truth_parent_eta;
  std::vector<float>   *track_truth_parent_pt;
  std::vector<float>   *track_truth_parent_e;
  std::vector<int>     *track_truth_parent_charge;
  std::vector<int>     *track_truth_parent_pdgid;
  std::vector<int>     *track_truth_parent_barcode;
  std::vector<int>     *track_truth_parent_nChildren;
  std::vector<float>   *track_truth_parent_decayr;

  // List of branches
  TBranch        *b_is_data;   //!
  TBranch        *b_runNumber;   //!
  TBranch        *b_lumiBlock;   //!
  TBranch        *b_eventNumber;   //!
  TBranch        *b_average_interaction_per_crossing;   //!
  TBranch        *b_actual_interaction_per_crossing;   //!
  TBranch        *b_has_pv;   //!
  TBranch        *b_pass_data_quality;   //!
  TBranch        *b_pass_event_cleaning;   //!
  TBranch        *b_nMuons;   //!
  TBranch        *b_nElectrons;   //!
  TBranch        *b_nJets;   //!
  TBranch        *b_nMSTracks;   //!
  TBranch        *b_nTracks;   //!
  TBranch        *b_nTruths;   //!
  TBranch        *b_muon_phi;   //!
  TBranch        *b_muon_eta;   //!
  TBranch        *b_muon_pt;   //!
  TBranch        *b_muon_e;   //!
  TBranch        *b_muon_charge;   //!
  TBranch        *b_electron_phi;   //!
  TBranch        *b_electron_eta;   //!
  TBranch        *b_electron_pt;   //!
  TBranch        *b_electron_e;   //!
  TBranch        *b_electron_charge;   //!
  TBranch        *b_jet_phi;   //!
  TBranch        *b_jet_eta;   //!
  TBranch        *b_jet_pt;   //!
  TBranch        *b_jet_e;   //!
  TBranch        *b_msTrack_phi;   //!
  TBranch        *b_msTrack_eta;   //!
  TBranch        *b_msTrack_pt;   //!
  TBranch        *b_msTrack_e;   //!
  TBranch        *b_msTrack_charge;   //!
  TBranch        *b_truth_phi;   //!
  TBranch        *b_truth_eta;   //!
  TBranch        *b_truth_pt;   //!
  TBranch        *b_truth_e;   //!
  TBranch        *b_truth_vis_phi;   //!
  TBranch        *b_truth_vis_eta;   //!
  TBranch        *b_truth_vis_pt;   //!
  TBranch        *b_truth_vis_e;   //!
  TBranch        *b_truth_charge;   //!
  TBranch        *b_truth_pdgid;   //!
  TBranch        *b_truth_barcode;   //!
  TBranch        *b_truth_nChildren;   //!
  TBranch        *b_truth_nTauProngs;   //!
  TBranch        *b_truth_nPi0;   //!
  TBranch        *b_truth_isTauHad;   //!
  TBranch        *b_truth_parent_barcode;   //!
  TBranch        *b_truth_properTime;   //!
  TBranch        *b_truth_decayr;   //!
  TBranch        *b_track_isTracklet;   //!
  TBranch        *b_track_phi;   //!
  TBranch        *b_track_eta;   //!
  TBranch        *b_track_pt;   //!
  TBranch        *b_track_e;   //!
  TBranch        *b_track_charge;   //!
  TBranch        *b_track_pixelBarrel_0;   //!
  TBranch        *b_track_pixelBarrel_1;   //!
  TBranch        *b_track_pixelBarrel_2;   //!
  TBranch        *b_track_pixelBarrel_3;   //!
  TBranch        *b_track_sctBarrel_0;   //!
  TBranch        *b_track_sctBarrel_1;   //!
  TBranch        *b_track_sctBarrel_2;   //!
  TBranch        *b_track_sctBarrel_3;   //!
  TBranch        *b_track_sctEndcap_0;   //!
  TBranch        *b_track_sctEndcap_1;   //!
  TBranch        *b_track_sctEndcap_2;   //!
  TBranch        *b_track_sctEndcap_3;   //!
  TBranch        *b_track_sctEndcap_4;   //!
  TBranch        *b_track_sctEndcap_5;   //!
  TBranch        *b_track_sctEndcap_6;   //!
  TBranch        *b_track_sctEndcap_7;   //!
  TBranch        *b_track_sctEndcap_8;   //!
  TBranch        *b_track_nPixHits;   //!
  TBranch        *b_track_nPixHoles;   //!
  TBranch        *b_track_nPixSharedHits;   //!
  TBranch        *b_track_nPixOutliers;   //!
  TBranch        *b_track_nPixDeadSensors;   //!
  TBranch        *b_track_nContribPixelLayers;   //!
  TBranch        *b_track_nGangedFlaggedFakes;   //!
  TBranch        *b_track_nPixelSpoiltHits;   //!
  TBranch        *b_track_nSCTHits;   //!
  TBranch        *b_track_nSCTHoles;   //!
  TBranch        *b_track_nSCTSharedHits;   //!
  TBranch        *b_track_nSCTOutliers;   //!
  TBranch        *b_track_nSCTDeadSensors;   //!
  TBranch        *b_track_nTRTHits;   //!
  TBranch        *b_track_nTRTHoles;   //!
  TBranch        *b_track_nTRTSharedHits;   //!
  TBranch        *b_track_nTRTDeadSensors;   //!
  TBranch        *b_track_nTRTOutliers;   //!
  TBranch        *b_track_dEdx;   //!
  TBranch        *b_track_dRJet20;   //!
  TBranch        *b_track_dRJet50;   //!
  TBranch        *b_track_dRMSTrack;   //!
  TBranch        *b_track_dRMuon;   //!
  TBranch        *b_track_dRElectron;   //!
  TBranch        *b_track_d0;   //!
  TBranch        *b_track_d0Sig;   //!
  TBranch        *b_track_z0;   //!
  TBranch        *b_track_z0PV;   //!
  TBranch        *b_track_z0PVSinTheta;   //!
  TBranch        *b_track_chi2Prob;   //!
  TBranch        *b_track_truth_phi;   //!
  TBranch        *b_track_truth_eta;   //!
  TBranch        *b_track_truth_pt;   //!
  TBranch        *b_track_truth_e;   //!
  TBranch        *b_track_truth_charge;   //!
  TBranch        *b_track_truth_pdgid;   //!
  TBranch        *b_track_truth_barcode;   //!
  TBranch        *b_track_truth_nChildren;   //!
  TBranch        *b_track_truth_properTime;   //!
  TBranch        *b_track_truth_decayr;   //!
  TBranch        *b_track_truth_matchingProb;   //!
  TBranch        *b_track_truth_parent_phi;   //!
  TBranch        *b_track_truth_parent_eta;   //!
  TBranch        *b_track_truth_parent_pt;   //!
  TBranch        *b_track_truth_parent_e;   //!
  TBranch        *b_track_truth_parent_charge;   //!
  TBranch        *b_track_truth_parent_pdgid;   //!
  TBranch        *b_track_truth_parent_barcode;   //!
  TBranch        *b_track_truth_parent_nChildren;   //!
  TBranch        *b_track_truth_parent_decayr;   //!

  MyAnalysis(TTree *tree=0);
  virtual ~MyAnalysis();
  virtual Int_t    Cut(Long64_t entry);
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop(std::string fin, std::string fout);
  virtual Bool_t   Notify();
  virtual void     Show(Long64_t entry = -1);

  // proxy_setter.C
  virtual void     proxy_setter();
  virtual void     struct_clear();

  // hist_manager.C
  virtual void     hist_def();
  virtual void     hist_write(std::string fout);

  // track_manager.C
  virtual void     fill_track_hist();
  virtual void     fill_truth_hist();


 private:

  TH1D* m_average_mu_lb;
  TH1D* m_nevents_lb;
  TH1D* m_nstdtracks_lb;
  TH1D* m_ntracklets_lb;
  TH1D* m_average_mu;
  TH1D* m_actual_mu;
  TH1D* m_stdtrack_pt;
  TH1D* m_stdtrack_eta;
  TH1D* m_stdtrack_phi;
  TH1D* m_tracklet_pt;
  TH1D* m_tracklet_eta;
  TH1D* m_tracklet_phi;
  TH1D* m_chargino_decayr_reco;
  TH1D* m_chargino_decayr_truth;

  struct track {
    int     isTracklet;
    float   phi;
    float   eta;
    float   pt;
    float   e;
    float   charge;
    int     pixelBarrel_0;
    int     pixelBarrel_1;
    int     pixelBarrel_2;
    int     pixelBarrel_3;
    int     sctBarrel_0;
    int     sctBarrel_1;
    int     sctBarrel_2;
    int     sctBarrel_3;
    int     sctEndcap_0;
    int     sctEndcap_1;
    int     sctEndcap_2;
    int     sctEndcap_3;
    int     sctEndcap_4;
    int     sctEndcap_5;
    int     sctEndcap_6;
    int     sctEndcap_7;
    int     sctEndcap_8;
    int     nPixHits;
    int     nPixHoles;
    int     nPixSharedHits;
    int     nPixOutliers;
    int     nPixDeadSensors;
    int     nContribPixelLayers;
    int     nGangedFlaggedFakes;
    int     nPixelSpoiltHits;
    int     nSCTHits;
    int     nSCTHoles;
    int     nSCTSharedHits;
    int     nSCTOutliers;
    int     nSCTDeadSensors;
    int     nTRTHits;
    int     nTRTHoles;
    int     nTRTSharedHits;
    int     nTRTDeadSensors;
    int     nTRTOutliers;
    float   dEdx;
    float   dRJet20;
    float   dRJet50;
    float   dRMSTrack;
    float   dRMuon;
    float   dRElectron;
    float   d0;
    float   d0Sig;
    float   z0;
    float   z0PV;
    float   z0PVSinTheta;
    float   chi2Prob;
    float   truth_phi;
    float   truth_eta;
    float   truth_pt;
    float   truth_e;
    int     truth_charge;
    int     truth_pdgid;
    int     truth_barcode;
    int     truth_nChildren;
    float   truth_properTime;
    float   truth_decayr;
    float   truth_matchingProb;
    float   truth_parent_phi;
    float   truth_parent_eta;
    float   truth_parent_pt;
    float   truth_parent_e;
    int     truth_parent_charge;
    int     truth_parent_pdgid;
    int     truth_parent_barcode;
    int     truth_parent_nChildren;
    float   truth_parent_decayr;
  };

  struct truth {
    float   phi;
    float   eta;
    float   pt;
    float   e;
    float   vis_phi;
    float   vis_eta;
    float   vis_pt;
    float   vis_e;
    int     charge;
    int     pdgid;
    int     barcode;
    int     nChildren;
    int     nTauProngs;
    int     nPi0;
    int     isTauHad;
    int     parent_barcode;
    float   properTime;
    float   decayr;
  };


  std::vector<struct track*>* tracks;
  std::vector<struct truth*>* truths;

};

#endif
