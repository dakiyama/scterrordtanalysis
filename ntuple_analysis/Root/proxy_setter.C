#include "/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/SCT_error/source/ntuple_analysis/MyAnalysis/MyAnalysis.h"

void MyAnalysis::proxy_setter() {
  tracks = new std::vector<struct track*>(nTracks);
  unsigned ii_track(0);
  for(auto &track : *tracks) {
    track = new struct track;
    track->isTracklet = track_isTracklet->at(ii_track);
    track->phi = track_phi->at(ii_track);
    track->eta = track_eta->at(ii_track);
    track->pt = track_pt->at(ii_track);
    track->e = track_e->at(ii_track);
    track->charge = track_charge->at(ii_track);
    track->pixelBarrel_0 = track_pixelBarrel_0->at(ii_track);
    track->pixelBarrel_1 = track_pixelBarrel_1->at(ii_track);
    track->pixelBarrel_2 = track_pixelBarrel_2->at(ii_track);
    track->pixelBarrel_3 = track_pixelBarrel_3->at(ii_track);
    track->sctBarrel_0 = track_sctBarrel_0->at(ii_track);
    track->sctBarrel_1 = track_sctBarrel_1->at(ii_track);
    track->sctBarrel_2 = track_sctBarrel_2->at(ii_track);
    track->sctBarrel_3 = track_sctBarrel_3->at(ii_track);
    track->sctEndcap_0 = track_sctEndcap_0->at(ii_track);
    track->sctEndcap_1 = track_sctEndcap_1->at(ii_track);
    track->sctEndcap_2 = track_sctEndcap_2->at(ii_track);
    track->sctEndcap_3 = track_sctEndcap_3->at(ii_track);
    track->sctEndcap_4 = track_sctEndcap_4->at(ii_track);
    track->sctEndcap_5 = track_sctEndcap_5->at(ii_track);
    track->sctEndcap_6 = track_sctEndcap_6->at(ii_track);
    track->sctEndcap_7 = track_sctEndcap_7->at(ii_track);
    track->sctEndcap_8 = track_sctEndcap_8->at(ii_track);
    track->nPixHits = track_nPixHits->at(ii_track);
    track->nPixHoles = track_nPixHoles->at(ii_track);
    track->nPixSharedHits = track_nPixSharedHits->at(ii_track);
    track->nPixOutliers = track_nPixOutliers->at(ii_track);
    track->nPixDeadSensors = track_nPixDeadSensors->at(ii_track);
    track->nContribPixelLayers = track_nContribPixelLayers->at(ii_track);
    track->nGangedFlaggedFakes = track_nGangedFlaggedFakes->at(ii_track);
    track->nPixelSpoiltHits = track_nPixelSpoiltHits->at(ii_track);
    track->nSCTHits = track_nSCTHits->at(ii_track);
    track->nSCTHoles = track_nSCTHoles->at(ii_track);
    track->nSCTSharedHits = track_nSCTSharedHits->at(ii_track);
    track->nSCTOutliers = track_nSCTOutliers->at(ii_track);
    track->nSCTDeadSensors = track_nSCTDeadSensors->at(ii_track);
    track->nTRTHits = track_nTRTHits->at(ii_track);
    track->nTRTHoles = track_nTRTHoles->at(ii_track);
    track->nTRTSharedHits = track_nTRTSharedHits->at(ii_track);
    track->nTRTDeadSensors = track_nTRTDeadSensors->at(ii_track);
    track->nTRTOutliers = track_nTRTOutliers->at(ii_track);
    track->dEdx = track_dEdx->at(ii_track);
    track->dRJet20 = track_dRJet20->at(ii_track);
    track->dRJet50 = track_dRJet50->at(ii_track);
    track->dRMSTrack = track_dRMSTrack->at(ii_track);
    track->dRMuon = track_dRMuon->at(ii_track);
    track->dRElectron = track_dRElectron->at(ii_track);
    track->d0 = track_d0->at(ii_track);
    track->d0Sig = track_d0Sig->at(ii_track);
    track->z0 = track_z0->at(ii_track);
    track->z0PV = track_z0PV->at(ii_track);
    track->z0PVSinTheta = track_z0PVSinTheta->at(ii_track);
    track->chi2Prob = track_chi2Prob->at(ii_track);
    if(!is_data){
      track->truth_phi = track_truth_phi->at(ii_track);
      track->truth_eta = track_truth_eta->at(ii_track);
      track->truth_pt = track_truth_pt->at(ii_track);
      track->truth_e = track_truth_e->at(ii_track);
      track->truth_charge = track_truth_charge->at(ii_track);
      track->truth_pdgid = track_truth_pdgid->at(ii_track);
      track->truth_barcode = track_truth_barcode->at(ii_track);
      track->truth_nChildren = track_truth_nChildren->at(ii_track);
      track->truth_properTime = track_truth_properTime->at(ii_track);
      track->truth_decayr = track_truth_decayr->at(ii_track);
      track->truth_matchingProb = track_truth_matchingProb->at(ii_track);
      track->truth_parent_phi = track_truth_parent_phi->at(ii_track);
      track->truth_parent_eta = track_truth_parent_eta->at(ii_track);
      track->truth_parent_pt = track_truth_parent_pt->at(ii_track);
      track->truth_parent_e = track_truth_parent_e->at(ii_track);
      track->truth_parent_charge = track_truth_parent_charge->at(ii_track);
      track->truth_parent_pdgid = track_truth_parent_pdgid->at(ii_track);
      track->truth_parent_barcode = track_truth_parent_barcode->at(ii_track);
      track->truth_parent_nChildren = track_truth_parent_nChildren->at(ii_track);
      track->truth_parent_decayr = track_truth_parent_decayr->at(ii_track);
     } else {
      track->truth_phi = -999;
      track->truth_eta = -999;
      track->truth_pt = -999;
      track->truth_e = -999;
      track->truth_charge = -999;
      track->truth_pdgid = -999;
      track->truth_barcode = -999;
      track->truth_nChildren = -999;
      track->truth_properTime = -999;
      track->truth_decayr = -999;
      track->truth_matchingProb = -999;
      track->truth_parent_phi = -999;
      track->truth_parent_eta = -999;
      track->truth_parent_pt = -999;
      track->truth_parent_e = -999;
      track->truth_parent_charge = -999;
      track->truth_parent_pdgid = -999;
      track->truth_parent_barcode = -999;
      track->truth_parent_nChildren = -999;
      track->truth_parent_decayr = -999;
    }
    ++ii_track;
  }

  truths = new std::vector<struct truth*>(nTruths);
  unsigned ii_truth(0);
  if(!is_data){
    for(auto &truth : *truths) {
      truth = new struct truth;
      truth->phi = truth_phi->at(ii_truth);
      truth->eta = truth_eta->at(ii_truth);
      truth->pt = truth_pt->at(ii_truth);
      truth->e = truth_e->at(ii_truth);
      truth->charge = truth_charge->at(ii_truth);
      truth->pdgid = truth_pdgid->at(ii_truth);
      truth->barcode = truth_barcode->at(ii_truth);
      truth->nChildren = truth_nChildren->at(ii_truth);
      truth->vis_phi = truth_vis_phi->at(ii_truth);
      truth->vis_eta = truth_vis_eta->at(ii_truth);
      truth->vis_pt = truth_vis_pt->at(ii_truth);
      truth->vis_e = truth_vis_e->at(ii_truth);
      truth->nTauProngs = truth_nTauProngs->at(ii_truth);
      truth->isTauHad = truth_isTauHad->at(ii_truth);
      truth->nPi0 = truth_nPi0->at(ii_truth); // new variable take care
      truth->parent_barcode = truth_parent_barcode->at(ii_truth);
      truth->properTime = truth_properTime->at(ii_truth);
      truth->decayr = truth_decayr->at(ii_truth);
      ++ii_truth;
    }
  }

  std::sort(tracks->begin()   , tracks->end()   , [](struct track*  a, struct track*  b){return a->pt > b->pt;});
}

void MyAnalysis::struct_clear(){

  if(tracks != nullptr){
    for(auto &track : *tracks){
      delete track;
      track = nullptr;
    }
  }
  if(truths != nullptr){
    for(auto &truth : *truths){
      delete truth;
      truth = nullptr;
    }
  }

  delete tracks;
  tracks = nullptr;
  delete truths;
  truths = nullptr;
}
