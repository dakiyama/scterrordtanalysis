// -*- C++ -*-

#ifndef MYANALYSISALG_H // Include guard
#define MYANALYSISALG_H

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "AsgTools/AsgMetadataTool.h"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"

class ITHistSvc;
class TH1D;
class TH2D;

class MyAnalysisAlg:public AthAnalysisAlgorithm
{
 public:
  MyAnalysisAlg(const std::string& name, ISvcLocator* pSvcLocator); // Constructor
  ServiceHandle<ITHistSvc> m_thistSvc;
  virtual StatusCode beginInputFile() override;
  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;
  virtual StatusCode execute() override;

  virtual bool fill_track_object(unsigned trackContainerKey);
  virtual bool fill_physics_object();
  virtual bool fill_event_info();
  virtual bool lumi_skim(uint32_t lumiBlock);
  virtual bool event_cleaning();
  virtual bool set_branch();
  virtual bool data_quality();
  virtual void delete_object();
  virtual void clear_vector();

  // truth manager
  virtual bool truth_manager();
  virtual int  fill_truth_object(const xAOD::TruthParticle* truth);
  virtual int  fill_truth_tau(const xAOD::TruthParticle* truth);
  virtual int  fill_truth_jet(const xAOD::Jet* truthJet);
  virtual void fill_truth_met();
  virtual void fill_pseudo_visible_object(const xAOD::TruthParticle* truth);
  virtual void truth_tester(const xAOD::TruthParticle* truth);
  virtual bool fill_calocell_object();

 private:
  TH1D* m_h_sctflag;

  std::string m_message;

  unsigned m_event_counter;
  const  float c;
  const  float unit_GeV;
  const  bool  m_all_truth_pass;

  TTree* m_tree;

  // event
  int      m_is_data;
  int      m_runNumber;
  int      m_lumiBlock;
  uint64_t m_eventNumber;
  float    m_average_interaction_per_crossing;
  float    m_actual_interaction_per_crossing;
  int      m_has_pv;
  int      m_has_bad_muon;
  int      m_has_cosmic_muon;
  int      m_pass_data_quality;
  int      m_pass_event_cleaning;
  float    m_pv_z;
  int      m_nMuons;
  int      m_nElectrons;
  int      m_nJets;
  int      m_nMSTracks;
  int      m_nTracks;
  int      m_nTruths;

  // muon
  std::vector<float>* m_muon_phi;
  std::vector<float>* m_muon_eta;
  std::vector<float>* m_muon_pt;
  std::vector<float>* m_muon_e;
  std::vector<int>* m_muon_charge;

  // electron
  std::vector<float>* m_electron_phi;
  std::vector<float>* m_electron_eta;
  std::vector<float>* m_electron_pt;
  std::vector<float>* m_electron_e;
  std::vector<int>* m_electron_charge;

  // tau
  std::vector<float>* m_tau_phi;
  std::vector<float>* m_tau_eta;
  std::vector<float>* m_tau_pt;
  std::vector<float>* m_tau_e;
  std::vector<int>* m_tau_charge;

  // jet
  std::vector<float>* m_jet_phi;
  std::vector<float>* m_jet_eta;
  std::vector<float>* m_jet_pt;
  std::vector<float>* m_jet_e;

  // MSTrack
  std::vector<float>* m_msTrack_phi;
  std::vector<float>* m_msTrack_eta;
  std::vector<float>* m_msTrack_pt;
  std::vector<float>* m_msTrack_e;
  std::vector<int>* m_msTrack_charge;

  // truth
  std::vector<float>* m_truth_phi;
  std::vector<float>* m_truth_eta;
  std::vector<float>* m_truth_pt;
  std::vector<float>* m_truth_e;
  std::vector<float>* m_truth_vis_phi;
  std::vector<float>* m_truth_vis_eta;
  std::vector<float>* m_truth_vis_pt;
  std::vector<float>* m_truth_vis_e;
  std::vector<int>* m_truth_charge;
  std::vector<int>* m_truth_pdgid;
  std::vector<int>* m_truth_barcode;
  std::vector<int>* m_truth_nChildren;
  std::vector<int>* m_truth_isTauHad;
  std::vector<int>* m_truth_nTauProngs;
  std::vector<int>* m_truth_nPi0;
  std::vector<int>* m_truth_parent_barcode;
  std::vector<int>* m_truth_parent_pdgid;
  std::vector<float>* m_truth_properTime;
  std::vector<float>* m_truth_decayr;

  // track
  std::vector<int>* m_track_isTracklet;
  std::vector<float>* m_track_phi;
  std::vector<float>* m_track_eta;
  std::vector<float>* m_track_pt;
  std::vector<float>* m_track_e;
  std::vector<float>* m_track_charge;
  std::vector<int>* m_track_pixelBarrel_0;
  std::vector<int>* m_track_pixelBarrel_1;
  std::vector<int>* m_track_pixelBarrel_2;
  std::vector<int>* m_track_pixelBarrel_3;
  std::vector<int>* m_track_sctBarrel_0;
  std::vector<int>* m_track_sctBarrel_1;
  std::vector<int>* m_track_sctBarrel_2;
  std::vector<int>* m_track_sctBarrel_3;
  std::vector<int>* m_track_sctEndcap_0;
  std::vector<int>* m_track_sctEndcap_1;
  std::vector<int>* m_track_sctEndcap_2;
  std::vector<int>* m_track_sctEndcap_3;
  std::vector<int>* m_track_sctEndcap_4;
  std::vector<int>* m_track_sctEndcap_5;
  std::vector<int>* m_track_sctEndcap_6;
  std::vector<int>* m_track_sctEndcap_7;
  std::vector<int>* m_track_sctEndcap_8;
  std::vector<int>* m_track_nPixHits;
  std::vector<int>* m_track_nPixHoles;
  std::vector<int>* m_track_nPixSharedHits;
  std::vector<int>* m_track_nPixOutliers;
  std::vector<int>* m_track_nPixDeadSensors;
  std::vector<int>* m_track_nContribPixelLayers;
  std::vector<int>* m_track_nGangedFlaggedFakes;
  std::vector<int>* m_track_nPixelSpoiltHits;
  std::vector<int>* m_track_nSCTHits;
  std::vector<int>* m_track_nSCTHoles;
  std::vector<int>* m_track_nSCTSharedHits;
  std::vector<int>* m_track_nSCTOutliers;
  std::vector<int>* m_track_nSCTDeadSensors;
  std::vector<int>* m_track_nTRTHits;
  std::vector<int>* m_track_nTRTHoles;
  std::vector<int>* m_track_nTRTSharedHits;
  std::vector<int>* m_track_nTRTDeadSensors;
  std::vector<int>* m_track_nTRTOutliers;
  std::vector<float>* m_track_dEdx;
  std::vector<float>* m_track_dRJet20;
  std::vector<float>* m_track_dRJet50;
  std::vector<float>* m_track_dRMSTrack;
  std::vector<float>* m_track_dRMuon;
  std::vector<float>* m_track_dRElectron;
  std::vector<float>* m_track_d0;
  std::vector<float>* m_track_d0Sig;
  std::vector<float>* m_track_z0;
  std::vector<float>* m_track_z0PV;
  std::vector<float>* m_track_z0PVSinTheta;
  std::vector<float>* m_track_chi2Prob;
  std::vector<float>* m_track_truth_phi;
  std::vector<float>* m_track_truth_eta;
  std::vector<float>* m_track_truth_pt;
  std::vector<float>* m_track_truth_e;
  std::vector<int>* m_track_truth_charge;
  std::vector<int>* m_track_truth_pdgid;
  std::vector<int>* m_track_truth_barcode;
  std::vector<int>* m_track_truth_nChildren;
  std::vector<float>* m_track_truth_properTime;
  std::vector<float>* m_track_truth_decayr;
  std::vector<float>* m_track_truth_matchingProb;
  std::vector<float>* m_track_truth_parent_phi;
  std::vector<float>* m_track_truth_parent_eta;
  std::vector<float>* m_track_truth_parent_pt;
  std::vector<float>* m_track_truth_parent_e;
  std::vector<int>* m_track_truth_parent_charge;
  std::vector<int>* m_track_truth_parent_pdgid;
  std::vector<int>* m_track_truth_parent_barcode;
  std::vector<int>* m_track_truth_parent_nChildren;
  std::vector<float>* m_track_truth_parent_decayr;

};

#endif // MYANALYSISALG_H
