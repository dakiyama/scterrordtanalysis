#-----------------------------------------------------------------------------
# Athena imports
#-----------------------------------------------------------------------------
from AthenaCommon.Constants import *
from AthenaCommon.AppMgr import theApp
from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

#-----------------------------------------------------------------------------
# Message Service
#-----------------------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = WARNING
import AthenaServices
AthenaServices.AthenaServicesConf.AthenaEventLoopMgr.OutputLevel = WARNING

#-----------------------------------------------------------------------------
# Input Datasets
#-----------------------------------------------------------------------------
ServiceMgr.EventSelector.InputCollections = [
    "/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/SCT_error/sample/data23_13p6TeV.00452799.physics_Main.merge.AOD.f1354_m2173/data23_13p6TeV.00452799.physics_Main.merge.AOD.f1354_m2173._lb1299._0004.1"
#    "/gpfs/fs8001/dakiyama/DTStudy/DTAnalysis/SCT_error/sample/data23_13p6TeV/DAOD_PHYS.33733587._000232.pool.root.1"
#    "/gpfs/fs8001/dakiyama/DEDX/DAOD_LLP2/sample/mc23_13p6TeV.522311.MGPy8EG_A14N23LO_CmN_HinoLL_300_0p2_MET50_run3.merge.AOD.e8532_e8528_s4173_s4114_r14622_r14663/AOD.33916601._000001.pool.root.1"
]
theApp.EvtMax = 100000 # -1 means all events

#-----------------------------------------------------------------------------
# Algorithms
#-----------------------------------------------------------------------------
from MyAnalysisAlg.MyAnalysisAlgConf import *

job += MyAnalysisAlg("MyAnalysisAlg1", OutputLevel = INFO)
#-----------------------------------------------------------------------------

#-----------------------------------------------------------------------------
# save ROOT histograms and Tuple
#-----------------------------------------------------------------------------
from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc()
ServiceMgr.THistSvc.Output = [ "MyAnalysisAlg DATAFILE='xtrack_Histograms.root' OPT='RECREATE'" ]

print (job)
#-----------------------------------------------------------------------------
