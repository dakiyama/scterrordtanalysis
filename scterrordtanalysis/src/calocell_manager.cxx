#include "MyAnalysisAlg/MyAnalysisAlg.h"
#include "xAODAssociations/TrackParticleClusterAssociationContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODCore/ShallowCopy.h"
bool MyAnalysisAlg::fill_calocell_object() {
  std::cout<<"===== fill_calocell_object ====="<<std::endl;

  xAOD::MuonContainer* muonContainer = nullptr;
  const xAOD::MuonContainer* muonContainer_copy = nullptr;
  if(!evtStore()->retrieve(muonContainer_copy, "Muons").isSuccess()) return false;
  std::pair<xAOD::MuonContainer*, xAOD::ShallowAuxContainer*> shallowcopy_muon = xAOD::shallowCopyContainer(*muonContainer_copy);
  muonContainer = shallowcopy_muon.first;
  unsigned nMuons = 0;
  if( muonContainer != nullptr ) {
    for(const auto &muon : *muonContainer){
      std::cout<<nMuons<<" th muon pt = "<<muon->pt() * unit_GeV<<", eta = "<<muon->eta()<<", "<<muon->phi()<<std::endl;
      ++nMuons;
    }
  }

  // Look at associated clusters
  std::string m_containerName = "InDetTrackParticles";
  if ( !evtStore()->contains<xAOD::TrackParticleClusterAssociationContainer>( m_containerName+"ClusterAssociations") ) {
    ATH_MSG_WARNING ("Couldn't retrieve TrackParticleClusterAssociations with key: " << m_containerName+"ClusterAssociations" );
    return false;
  }
  
  const xAOD::TrackParticleClusterAssociationContainer* associatedClusters = 0;
  if( !evtStore()->retrieve( associatedClusters, m_containerName+"ClusterAssociations") ) {
    std::cout<<"failed to retrieve ClusterAssociation container"<<std::endl;
  };
  //ATH_MSG_INFO ("#(InDetTrackParticlesClusterAssociations) = " << associatedClusters->size());
  for ( const auto* assocClusters : *associatedClusters ) {
    ATH_MSG_DEBUG ("InDetTrackParticlesClusterAssociations index = " << assocClusters->index());
    // flollow the link to the track particle
    const xAOD::TrackParticle* trk = 0;
    if (assocClusters->trackParticleLink().isValid()) {
      trk = *(assocClusters->trackParticleLink());
      std::cout<<"ok, found a track particle link, pt = "<<trk->pt()*0.001<<", eta = "<<trk->eta()<<", phi = "<<trk->phi()<<std::endl;
    }
    else if ( !assocClusters->trackParticleLink().isValid() ){
      ATH_MSG_ERROR ("trackParticleLink is not valid! " );
    }

    // std::cout<<"size of associated clusters = "<<assocClusters->caloClusterLinks().size()<<std::endl;
    // for(const auto &assocCluster : assocClusters->caloClusterLinks()) {
    //   if (assocCluster.isValid()) {
    // 	const CaloClusterCellLink* cellLinks = cluster->getCellLinks();
    // 	trk = *(assocCluster.caloClusterLink());
    // 	std::cout<<"ok, found a track particle link"<<std::endl;
    //   }
    //   else if ( assocCluster.caloClusterLink().isValid() ){
    // 	ATH_MSG_ERROR ("caloClusterLink is not valid! " );
    //   }
    // }
  }

  return true;
}
