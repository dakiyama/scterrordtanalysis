#include "MyAnalysisAlg/MyAnalysisAlg.h"
#include "TMath.h"

MyAnalysisAlg::MyAnalysisAlg(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAnalysisAlgorithm(name, pSvcLocator),
    m_thistSvc("THistSvc/THistSvc", name),
    c(TMath::C() / (1e-3)*(1e-9)),
    unit_GeV(0.001),
    m_all_truth_pass(false)
{
  declareProperty("message", m_message="You cannot always get what you want.");
}

StatusCode MyAnalysisAlg::initialize() {
  ATH_MSG_INFO("initialize()");
  ATH_MSG_INFO("My message: " << m_message);
  ATH_CHECK(m_thistSvc.retrieve());
  set_branch();
  ATH_CHECK(m_thistSvc->regTree("/MyAnalysisAlg/MyTree", m_tree));
  m_tree->Print();
  m_event_counter = 0;

  return StatusCode::SUCCESS;
}

StatusCode MyAnalysisAlg::finalize() {
  ATH_MSG_INFO("finalize()");
  delete_object();
  return StatusCode::SUCCESS;
}

StatusCode MyAnalysisAlg::beginInputFile() {
  ATH_MSG_INFO("beginInputFile()");
  return StatusCode::SUCCESS;
}

StatusCode MyAnalysisAlg::execute() {
  if(m_event_counter % 1000 == 0) ATH_MSG_INFO("execute() -----> "<<m_event_counter<<" events");
  m_event_counter++;
  clear_vector();

  if( data_quality() == true ) m_pass_data_quality = 1;
  if( fill_event_info() == false ) return StatusCode::SUCCESS;
  event_cleaning();

  fill_event_info();
  fill_calocell_object();
  // fill_physics_object();
  if( !m_is_data ) truth_manager();
  if( !fill_track_object(0) ) return StatusCode::SUCCESS;
  if( !fill_track_object(1) ) return StatusCode::SUCCESS;

  int nbytes = m_tree->Fill();
  ATH_MSG_DEBUG("Number of bytes written = " << nbytes);

  return StatusCode::SUCCESS;
}
