#include "MyAnalysisAlg/MyAnalysisAlg.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODCore/ShallowCopy.h"

bool MyAnalysisAlg::fill_physics_object() {
  xAOD::MuonContainer* muonContainer = nullptr;
  const xAOD::MuonContainer* muonContainer_copy = nullptr;
  if(!evtStore()->retrieve(muonContainer_copy, "Muons").isSuccess()) return false;
  std::pair<xAOD::MuonContainer*, xAOD::ShallowAuxContainer*> shallowcopy_muon = xAOD::shallowCopyContainer(*muonContainer_copy);
  muonContainer = shallowcopy_muon.first;

  xAOD::ElectronContainer* electronContainer = nullptr;
  const xAOD::ElectronContainer* electronContainer_copy = nullptr;
  if(!evtStore()->retrieve(electronContainer_copy, "Electrons").isSuccess()) return false;
  std::pair<xAOD::ElectronContainer*, xAOD::ShallowAuxContainer*> shallowcopy_ele = xAOD::shallowCopyContainer(*electronContainer_copy);
  electronContainer = shallowcopy_ele.first;

  xAOD::JetContainer* jetContainer = nullptr;
  const xAOD::JetContainer* jetContainer_copy = nullptr;
  if(!evtStore()->retrieve(jetContainer_copy, "AntiKt4EMTopoJets")) return false;
  std::pair<xAOD::JetContainer*, xAOD::ShallowAuxContainer*> shallowcopy_jet = xAOD::shallowCopyContainer(*jetContainer_copy);
  jetContainer = shallowcopy_jet.first;

  // muon
  unsigned nMuons = 0;
  if( muonContainer != nullptr ) {
    for(const auto &muon : *muonContainer){
      if (muon->SG::AuxElement::auxdata<char>("passOR") != 1) continue;
      if (muon->SG::AuxElement::auxdata<char>("cosmic") == 1) m_has_cosmic_muon = 1;
      if (muon->SG::AuxElement::auxdata<char>("bad") == 1) m_has_bad_muon = 1;
      if (muon->SG::AuxElement::auxdata<char>("baseline") != 1) continue;
      m_muon_phi->push_back(muon->phi());
      m_muon_eta->push_back(muon->eta());
      m_muon_pt->push_back(muon->pt() * unit_GeV);
      m_muon_e->push_back(muon->e() * unit_GeV);
      if (muon->primaryTrackParticle()) {
	m_muon_charge->push_back(muon->primaryTrackParticle()->charge());
      } else {
	m_muon_charge->push_back(0.);
      }
      ++nMuons;
    }
  }  m_nMuons = static_cast<int>(nMuons);

  // electron
  unsigned nElectrons = 0;
  if( electronContainer != nullptr ) {
    for(const auto &electron : *electronContainer){
      if (electron->SG::AuxElement::auxdata<char>("passOR") != 1) continue;
      if (electron->SG::AuxElement::auxdata<char>("baseline") != 1) continue;
      m_electron_phi->push_back(electron->phi());
      m_electron_eta->push_back(electron->eta());
      m_electron_pt->push_back(electron->pt() * unit_GeV);
      m_electron_e->push_back(electron->e() * unit_GeV);
      m_electron_charge->push_back(electron->charge());
      ++nElectrons;
    }
  }
  m_nElectrons = static_cast<int>(nElectrons);

  // jet
  unsigned nJets = 0;
  for(const auto &jet : *jetContainer){
    m_jet_phi->push_back(jet->phi());
    m_jet_eta->push_back(jet->eta());
    m_jet_pt->push_back(jet->pt() * unit_GeV);
    m_jet_e->push_back(jet->e() * unit_GeV);
    ++nJets;
  }
  m_nJets = static_cast<int>(nJets);

  // MS track
  unsigned nMSTracks = 0;
  const xAOD::TrackParticleContainer* msTrackContainer = nullptr;
  if (evtStore()->retrieve(msTrackContainer, "MuonSpectrometerTrackParticles").isSuccess()) {
    for (const auto &track : *msTrackContainer) {
      m_msTrack_phi->push_back(track->phi());
      m_msTrack_eta->push_back(track->eta());
      m_msTrack_pt->push_back(track->pt() * unit_GeV);
      m_msTrack_e->push_back(track->e() * unit_GeV);
      ++nMSTracks;
    }
  }
  m_nMSTracks = static_cast<int>(nMSTracks);

  // pass event cleaning
  if( m_has_cosmic_muon == 0 && m_has_bad_muon == 0 )
    m_pass_event_cleaning = 1;
  else {
    m_pass_event_cleaning = 0;
    return false;
  }

  return true;
}
