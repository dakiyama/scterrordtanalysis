#include "MyAnalysisAlg/MyAnalysisAlg.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include <iomanip>

bool MyAnalysisAlg::data_quality() {
  ATH_MSG_DEBUG("data_quality()");
  const xAOD::EventInfo* eventInfo = nullptr;
  if(!evtStore()->retrieve(eventInfo).isSuccess()) return false;
  m_is_data = !eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);
  // if(!m_grlTool->passRunLB(*eventInfo)) return false;
  if (eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) return false;
  if (eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) return false;
  if (eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) {
    m_h_sctflag->Fill(m_eventNumber);
    //std::cout << std::fixed << std::setprecision(15) <<m_eventNumber<<std::endl;
    //return false;
  }
  if (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) return false;
  return true;
}

bool MyAnalysisAlg::event_cleaning() {
  ATH_MSG_DEBUG("event_cleaning()");

  // Primary Vertex
  const xAOD::VertexContainer* vertexContainer = nullptr;
  if (evtStore()->retrieve(vertexContainer, "PrimaryVertices").isSuccess()) {
    for (const auto &vertex : *vertexContainer) {
      if (vertex->vertexType() == xAOD::VxType::PriVtx && vertex->nTrackParticles() >= 2) {
	m_has_pv = 1;
	m_pv_z = vertex->z();
      }
    }
  }

  //if(m_has_pv == 0) return false;
  return true;
}

bool MyAnalysisAlg::fill_event_info() {
  const xAOD::EventInfo* eventInfo = nullptr;
  if(!evtStore()->retrieve(eventInfo).isSuccess()) return false;
  uint32_t runNumber = eventInfo->runNumber();
  uint32_t lumiBlock = eventInfo->lumiBlock();
  uint64_t eventNumber = eventInfo->eventNumber();
  m_runNumber          = runNumber;
  m_lumiBlock          = lumiBlock;
  m_eventNumber        = eventNumber;
  m_average_interaction_per_crossing           = eventInfo->averageInteractionsPerCrossing();
  m_actual_interaction_per_crossing            = eventInfo->actualInteractionsPerCrossing();

  bool is_hv_scan = lumi_skim(lumiBlock);
  return is_hv_scan;

  //return true;
}

bool MyAnalysisAlg::lumi_skim(uint32_t lumiBlock) {
  if(lumiBlock >= 538 && lumiBlock <= 743) return true;
  if(lumiBlock >= 756 && lumiBlock <= 899) return true;
  return false;
}
