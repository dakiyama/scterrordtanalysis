#include "MyAnalysisAlg/MyAnalysisAlg.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODJet/JetContainer.h"

int MyAnalysisAlg::fill_truth_object(const xAOD::TruthParticle *truth) {
float decayR = 0.;
float proper_time = 0.;
 m_truth_phi->push_back(truth->phi());
 m_truth_eta->push_back(truth->eta());
 m_truth_pt->push_back(truth->pt() * unit_GeV);
 m_truth_e->push_back(truth->e() * unit_GeV);
 m_truth_charge->push_back(truth->charge());
 m_truth_pdgid->push_back(truth->pdgId());
 m_truth_barcode->push_back(truth->barcode());
 m_truth_nChildren->push_back(truth->nChildren());
 if(truth->parent() != nullptr) {
   m_truth_parent_pdgid->push_back(truth->parent(0)->pdgId());
   m_truth_parent_barcode->push_back(truth->parent(0)->barcode());
 } else {
   m_truth_parent_pdgid->push_back(-999);
   m_truth_parent_barcode->push_back(-999);
 }
  
 decayR = truth->decayVtx() != nullptr? truth->decayVtx()->perp() : -999.;
 auto bFactor_t = truth->pt() / truth->m();
 proper_time = decayR / bFactor_t / c;
 m_truth_properTime->push_back(proper_time);
 m_truth_decayr->push_back(decayR);
 return 1;
}

int MyAnalysisAlg::fill_truth_tau(const xAOD::TruthParticle *truth) {
  bool exist_taunu = false;
  unsigned n_tau_prongs(0);
  unsigned n_pi0(0);
  unsigned is_tau_had(1);
  if( std::fabs(truth->pdgId()) == 15 && truth->status() == 2 ){
    TLorentzVector taunu(0., 0., 0., 0.), enu(0., 0., 0., 0.), munu(0., 0., 0., 0.), tauvis(0., 0., 0., 0.);
    if(truth->nChildren() == 0) {
      ATH_MSG_DEBUG("this is no child tau. continue this loop.");
      return 0;
    }
    // truth_tester(truth);
    for(unsigned int ii_tau_ch = 0; ii_tau_ch < truth->nChildren(); ii_tau_ch++){
      const xAOD::TruthParticle* tau_child = truth->child(ii_tau_ch);
      if( std::fabs(tau_child->pdgId()) == 12 ) {
	enu = tau_child->p4();
      }
      if( std::fabs(tau_child->pdgId()) == 14 ) {
	munu = tau_child->p4();
      }
      if( std::fabs(tau_child->pdgId()) == 16 ) {
	taunu = tau_child->p4();
	exist_taunu = true;
      }
      if( std::fabs(tau_child->pdgId()) == 15 ) {
	ATH_MSG_DEBUG("this tau has child tau. break this loop.");
	break;
      }
      if( tau_child->status() == 1 && std::fabs(tau_child->pdgId()) == 211 ) ++n_tau_prongs;
      if( tau_child->status() == 1 && std::fabs(tau_child->pdgId()) == 321 ) ++n_tau_prongs;
      if( tau_child->status() == 1 && std::fabs(tau_child->pdgId()) == 323 ) ++n_tau_prongs;
      if( tau_child->status() == 1 && std::fabs(tau_child->pdgId()) == 10323 ) ++n_tau_prongs;
      if( tau_child->status() == 1 && std::fabs(tau_child->pdgId()) == 20323 ) ++n_tau_prongs;
      if( tau_child->status() == 1 && std::fabs(tau_child->pdgId()) == 113 ) n_tau_prongs += 2;
      if( tau_child->status() == 1 && std::fabs(tau_child->pdgId()) == 11) is_tau_had = 0;
      if( tau_child->status() == 1 && std::fabs(tau_child->pdgId()) == 13) is_tau_had = 0;
      if( tau_child->status() == 2 && tau_child->pdgId() == 111) ++n_pi0;
    }
    if(exist_taunu == false) {
      ATH_MSG_DEBUG("this tau decays but no tau nuetrino. continue this loop.");
      return 0;
    }
    if(n_tau_prongs % 2 == 0 && is_tau_had == 1){
      std::cout<<"n tau prongs = "<<n_tau_prongs<<std::endl;
      for(unsigned int ii_tau_ch = 0; ii_tau_ch < truth->nChildren(); ii_tau_ch++){
	const xAOD::TruthParticle* tau_child = truth->child(ii_tau_ch);
	std::cout<<"tau child pdgId = "<<tau_child->pdgId()<<", status = "<<tau_child->status()<<", barcode = "<<tau_child->barcode()<<std::endl;
      }
    }
    tauvis = truth->p4() - taunu - enu - munu;
    fill_truth_object(truth);
    m_truth_vis_phi->push_back(tauvis.Phi());
    m_truth_vis_eta->push_back(tauvis.Eta());
    m_truth_vis_pt->push_back(tauvis.Pt() * unit_GeV);
    m_truth_vis_e->push_back(tauvis.E() * unit_GeV);
    m_truth_nTauProngs->push_back(n_tau_prongs);
    m_truth_nPi0->push_back(n_pi0);
    m_truth_isTauHad->push_back(is_tau_had);
  }
  return 1;
}

void MyAnalysisAlg::fill_pseudo_visible_object(const xAOD::TruthParticle *truth) {
  m_truth_vis_phi->push_back(truth->phi());
  m_truth_vis_eta->push_back(truth->eta());
  m_truth_vis_pt->push_back(truth->pt() * unit_GeV);
  m_truth_vis_e->push_back(truth->e() * unit_GeV);
  m_truth_nTauProngs->push_back(-1.);
  m_truth_nPi0->push_back(-1.);
  m_truth_isTauHad->push_back(-1.);
}

int MyAnalysisAlg::fill_truth_jet(const xAOD::Jet *truthJet) {
  // m_truthjet_phi->push_back(truthJet->phi());
  // m_truthjet_eta->push_back(truthJet->eta());
  // m_truthjet_pt->push_back(truthJet->pt() * unit_GeV);
  // m_truthjet_e->push_back(truthJet->e() * unit_GeV);
  return 1;
}

bool MyAnalysisAlg::truth_manager(){
  unsigned nTruths = 0;
  const xAOD::TruthParticleContainer* truthContainer = nullptr;
  if (evtStore()->retrieve(truthContainer, "TruthParticles").isSuccess()) {
    for(const auto &truth : *truthContainer){
      if( m_all_truth_pass ) {
	if( std::fabs(truth->pdgId()) == 15 && truth->status() == 2 ) nTruths += fill_truth_tau(truth);
	else if( std::fabs(truth->pdgId()) != 15 && truth->status() == 1) {
	  nTruths += fill_truth_object(truth);
	  fill_pseudo_visible_object(truth);
	}
	continue;
      }

      // if( truth->pt()/1000. < 5. ) continue;

      if( truth->status() == 1 && 
	  (std::fabs(truth->pdgId()) == 1000024 )) { // chargino
	// std::fabs(truth->pdgId()) == 211 ||     // pi+-
	// std::fabs(truth->pdgId()) == 2212 ||    // proton
	// std::fabs(truth->pdgId()) == 3222 ||    // sigma+
	// std::fabs(truth->pdgId()) == 11 ||      // electron
	// std::fabs(truth->pdgId()) == 13) ) {    // muon
	nTruths += fill_truth_object(truth);
	fill_pseudo_visible_object(truth);
      }

      bool is_z(false);
      bool is_w(false);

      if( truth->pdgId() == 23 && truth->nChildren() > 1 ) {
	is_z = true;
	nTruths += fill_truth_object(truth);
	fill_pseudo_visible_object(truth);
	for(unsigned int ii_z_ch = 0; ii_z_ch < truth->nChildren(); ii_z_ch++){
	  const xAOD::TruthParticle* z_child = truth->child(ii_z_ch);
	  if( (std::fabs(z_child->pdgId()) == 11 || std::fabs(z_child->pdgId()) == 13) && z_child->status() == 1 ) {
	    nTruths += fill_truth_object(z_child);
	    fill_pseudo_visible_object(z_child);
	  }
	  if( std::fabs(z_child->pdgId()) == 15 && z_child->status() == 2 ) nTruths += fill_truth_tau(z_child);
	}
      }

      // W->lnu
      if( std::fabs(truth->pdgId()) == 24 && truth->nChildren() > 1 ) {
	is_w = true;
	nTruths += fill_truth_object(truth);
	fill_pseudo_visible_object(truth);
	for(unsigned int ii_w_ch = 0; ii_w_ch < truth->nChildren(); ii_w_ch++){
	  const xAOD::TruthParticle* w_child = truth->child(ii_w_ch);
	  if( (std::fabs(w_child->pdgId()) == 11 || std::fabs(w_child->pdgId()) == 12 || std::fabs(w_child->pdgId()) == 13 || std::fabs(w_child->pdgId()) == 14 || std::fabs(w_child->pdgId()) == 16) && w_child->status() == 1 ) {
	    nTruths += fill_truth_object(w_child);
	    fill_pseudo_visible_object(w_child);
	  }
	  if( std::fabs(w_child->pdgId()) == 15 && w_child->status() == 2 ) nTruths += fill_truth_tau(w_child);
	}
      }

      if(!is_z && !is_w){
	if( std::fabs(truth->pdgId()) == 15 && truth->status() == 2 ) nTruths += fill_truth_tau(truth);
      }
    }
  }
  m_nTruths = static_cast<int>(nTruths);

  // const xAOD::JetContainer* truthJetContainer = nullptr;
  // if (evtStore()->retrieve(truthJetContainer, "AntiKt4TruthJets").isSuccess()) {
  //   for(const auto truthJet : *truthJetContainer) {
  //     m_nTruthJets += fill_truth_jet(truthJet);
  //   }
  // }

  return true;
}

void MyAnalysisAlg::fill_truth_met(){
  // xAOD::MissingETContainer* missingETContainer = new xAOD::MissingETContainer;
  // if(evtStore()->retrieve(missingETContainer, "MET_Truth").isSuccess()) {
  // }
  // delete missingETContainer;
  // missingETContainer = nullptr;
}

void MyAnalysisAlg::truth_tester(const xAOD::TruthParticle *truth) {
  if( std::fabs(truth->pdgId()) == 15 ){
    std::cout<<"===================="<<std::endl<<"# tau"<<
      " pdgId : " << truth->pdgId() << 
      " status : " << truth->status() << 
      " barcode : " << truth->barcode() << 
      " nChildren : " << truth->nChildren() << 
      " Pt : " << truth->pt() <<
      " Eta : " << truth->eta() <<
      " Phi : " << truth->phi() <<
      " E : " << truth->e() <<
      " M : " << truth->m()<<std::endl;
    for(unsigned int ii_ch = 0; ii_ch < truth->nChildren(); ii_ch++){
      const xAOD::TruthParticle* child = truth->child(ii_ch);
      std::cout<<"### "<< ii_ch << "th child :" <<
	" pdgId : " << child->pdgId() << 
	" status : " << child->status() << 
	" barcode : " << child->barcode() << 
	" nChildren : " << child->nChildren() << 
	" Pt : " << child->pt() <<
	" Eta : " << child->eta() <<
	" Phi : " << child->phi() <<
	" E : " << child->e() <<
	" M : " << child->m()<<std::endl;
      if( std::fabs(truth->charge()) == 1 ){
	for(unsigned int ii_gch = 0; ii_gch < child->nChildren(); ii_gch++){
	  const xAOD::TruthParticle* grand_child = child->child(ii_gch);
	  std::cout<<"##### "<< ii_gch << "th child :" <<
	    " pdgId : " << grand_child->pdgId() << 
	    " status : " << grand_child->status() << 
	    " barcode : " << grand_child->barcode() << 
	    " nChildren : " << grand_child->nChildren() << 
	    " Pt : " << grand_child->pt() <<
	    " Eta : " << grand_child->eta() <<
	    " Phi : " << grand_child->phi() <<
	    " E : " << grand_child->e() <<
	    " M : " << grand_child->m()<<std::endl;
	}
      }
    }
    std::cout << "====================" << std::endl;
  }      
}
