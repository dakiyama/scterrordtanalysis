#include "MyAnalysisAlg/MyAnalysisAlg.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"

bool MyAnalysisAlg::fill_track_object(unsigned trackContainerKey) {
  float decayR = 0.;
  float proper_time = 0.;
  unsigned int nTracks = 0;
  const xAOD::TrackParticleContainer* trackContainer = nullptr;
  std::string trackContainerName;
  if(trackContainerKey == 0) trackContainerName = "InDetTrackParticles";
  if(trackContainerKey == 1) trackContainerName = "InDetDisappearingTrackParticles";
  if (evtStore()->retrieve(trackContainer, trackContainerName.c_str()).isSuccess()) {
    for(const auto &track : *trackContainer){

      // hit summary
      uint8_t nPixHits            = 0;
      uint8_t nPixHoles           = 0;
      uint8_t nPixSharedHits      = 0;
      uint8_t nPixOutliers        = 0;
      uint8_t nPixDeadSensors     = 0;
      uint8_t nContribPixelLayers = 0;
      uint8_t nGangedFlaggedFakes = 0;
      uint8_t nPixelSpoiltHits    = 0;
      uint8_t nSCTHits            = 0;
      uint8_t nSCTHoles           = 0;
      uint8_t nSCTSharedHits      = 0;
      uint8_t nSCTOutliers        = 0;
      uint8_t nSCTDeadSensors     = 0;
      uint8_t nTRTHits            = 0;
      uint8_t nTRTHoles           = 0;
      uint8_t nTRTSharedHits      = 0;
      uint8_t nTRTDeadSensors     = 0;
      uint8_t nTRTOutliers        = 0;
      float   dEdx                = 0.0;
      track->summaryValue(nPixHits,              xAOD::numberOfPixelHits);
      track->summaryValue(nPixHoles,             xAOD::numberOfPixelHoles);
      track->summaryValue(nPixSharedHits,        xAOD::numberOfPixelSharedHits);
      track->summaryValue(nPixOutliers,          xAOD::numberOfPixelOutliers);
      track->summaryValue(nPixDeadSensors,       xAOD::numberOfPixelDeadSensors);
      track->summaryValue(nContribPixelLayers,   xAOD::numberOfContribPixelLayers);
      track->summaryValue(nGangedFlaggedFakes,   xAOD::numberOfGangedFlaggedFakes);
      track->summaryValue(nPixelSpoiltHits,      xAOD::numberOfPixelSpoiltHits);
      track->summaryValue(nSCTHits,              xAOD::numberOfSCTHits);
      track->summaryValue(nSCTHoles,             xAOD::numberOfSCTHoles);
      track->summaryValue(nSCTSharedHits,        xAOD::numberOfSCTSharedHits);
      track->summaryValue(nSCTOutliers,          xAOD::numberOfSCTOutliers);
      track->summaryValue(nSCTDeadSensors,       xAOD::numberOfSCTDeadSensors);
      track->summaryValue(nTRTHits,              xAOD::numberOfTRTHits);
      track->summaryValue(nTRTHoles,             xAOD::numberOfTRTHoles);
      track->summaryValue(nTRTSharedHits,        xAOD::numberOfTRTSharedHits);
      track->summaryValue(nTRTOutliers,          xAOD::numberOfTRTOutliers);
      track->summaryValue(nTRTDeadSensors,       xAOD::numberOfTRTDeadStraws);
      track->summaryValue(dEdx,                  xAOD::pixeldEdx);

      //if(track->pt() * unit_GeV > 10.) {} else continue;
      //if(nGangedFlaggedFakes == 0) {} else continue;
      //if(nPixelSpoiltHits == 0) {} else continue;
      //if(nPixOutliers == 0) {} else continue;
      //if(nContribPixelLayers >= 4) {} else continue;

      ++nTracks;

      m_track_phi->push_back(track->phi());
      m_track_eta->push_back(track->eta());
      m_track_pt->push_back(track->pt() * unit_GeV);
      m_track_e->push_back(track->e() * unit_GeV);
      m_track_charge->push_back(track->charge());
      m_track_isTracklet->push_back(trackContainerKey);


      // hit pattern
      static SG::AuxElement::Accessor<uint32_t> acc_hitPattern("hitPattern");
      if(acc_hitPattern.isAvailable(*track)){
	std::bitset<25> hitPattern = track->hitPattern();
	m_track_pixelBarrel_0->push_back(hitPattern[0]);
	m_track_pixelBarrel_1->push_back(hitPattern[1]);
	m_track_pixelBarrel_2->push_back(hitPattern[2]);
	m_track_pixelBarrel_3->push_back(hitPattern[3]);
	m_track_sctBarrel_0->push_back(hitPattern[7]);
	m_track_sctBarrel_1->push_back(hitPattern[8]);
	m_track_sctBarrel_2->push_back(hitPattern[9]);
	m_track_sctBarrel_3->push_back(hitPattern[10]);
	m_track_sctEndcap_0->push_back(hitPattern[11]);
	m_track_sctEndcap_1->push_back(hitPattern[12]);
	m_track_sctEndcap_2->push_back(hitPattern[13]);
	m_track_sctEndcap_3->push_back(hitPattern[14]);
	m_track_sctEndcap_4->push_back(hitPattern[15]);
	m_track_sctEndcap_5->push_back(hitPattern[16]);
	m_track_sctEndcap_6->push_back(hitPattern[17]);
	m_track_sctEndcap_7->push_back(hitPattern[18]);
	m_track_sctEndcap_8->push_back(hitPattern[19]);
      } else {
	m_track_pixelBarrel_0->push_back(0);
	m_track_pixelBarrel_1->push_back(0);
	m_track_pixelBarrel_2->push_back(0);
	m_track_pixelBarrel_3->push_back(0);
	m_track_sctBarrel_0->push_back(0);
	m_track_sctBarrel_1->push_back(0);
	m_track_sctBarrel_2->push_back(0);
	m_track_sctBarrel_3->push_back(0);
	m_track_sctEndcap_0->push_back(0);
	m_track_sctEndcap_1->push_back(0);
	m_track_sctEndcap_2->push_back(0);
	m_track_sctEndcap_3->push_back(0);
	m_track_sctEndcap_4->push_back(0);
	m_track_sctEndcap_5->push_back(0);
	m_track_sctEndcap_6->push_back(0);
	m_track_sctEndcap_7->push_back(0);
	m_track_sctEndcap_8->push_back(0);
      }

      m_track_nPixHits->push_back(nPixHits);
      m_track_nPixHoles->push_back(nPixHoles);
      m_track_nPixSharedHits->push_back(nPixSharedHits);
      m_track_nPixOutliers->push_back(nPixOutliers);
      m_track_nPixDeadSensors->push_back(nPixDeadSensors);
      m_track_nContribPixelLayers->push_back(nContribPixelLayers);
      m_track_nGangedFlaggedFakes->push_back(nGangedFlaggedFakes);
      m_track_nPixelSpoiltHits->push_back(nPixelSpoiltHits);
      m_track_nSCTHits->push_back(nSCTHits);
      m_track_nSCTHoles->push_back(nSCTHoles);
      m_track_nSCTSharedHits->push_back(nSCTSharedHits);
      m_track_nSCTOutliers->push_back(nSCTOutliers);
      m_track_nSCTDeadSensors->push_back(nSCTDeadSensors);
      m_track_nTRTHits->push_back(nTRTHits);
      m_track_nTRTHoles->push_back(nTRTHoles);
      m_track_nTRTSharedHits->push_back(nTRTSharedHits);
      m_track_nTRTDeadSensors->push_back(nTRTDeadSensors);
      m_track_nTRTOutliers->push_back(nTRTOutliers);
      m_track_dEdx->push_back(dEdx);

      // overlap removal
      float min_muon_dR = 999.;
      for(unsigned int ii_muon = 0; ii_muon < static_cast<unsigned int>(m_nMuons); ++ii_muon){
	float dPhi = TVector2::Phi_mpi_pi(m_muon_phi->at(ii_muon) - track->phi());
	float dEta = m_muon_eta->at(ii_muon) - track->eta();
	float dR = std::sqrt(dPhi*dPhi + dEta*dEta);
	if(dR < min_muon_dR) min_muon_dR = dR;
      }
      m_track_dRMuon->push_back(min_muon_dR);

      float min_electron_dR = 999.;
      for(unsigned int ii_electron = 0; ii_electron < static_cast<unsigned int>(m_nElectrons); ++ii_electron){
	float dPhi = TVector2::Phi_mpi_pi(m_electron_phi->at(ii_electron) - track->phi());
	float dEta = m_electron_eta->at(ii_electron) - track->eta();
	float dR = std::sqrt(dPhi*dPhi + dEta*dEta);
	if(dR < min_electron_dR) min_electron_dR = dR;
      }
      m_track_dRElectron->push_back(min_electron_dR);

      float min_msTrack_dR = 999.;
      for(unsigned int ii_msTrack = 0; ii_msTrack < static_cast<unsigned int>(m_nMSTracks); ++ii_msTrack){
	float dPhi = TVector2::Phi_mpi_pi(m_msTrack_phi->at(ii_msTrack) - track->phi());
	float dEta = m_msTrack_eta->at(ii_msTrack) - track->eta();
	float dR = std::sqrt(dPhi*dPhi + dEta*dEta);
	if(dR < min_msTrack_dR) min_msTrack_dR = dR;
      }
      m_track_dRMSTrack->push_back(min_msTrack_dR);

      float min_jet20_dR = 999.;
      float min_jet50_dR = 999.;
      for(unsigned int ii_jet = 0; ii_jet < static_cast<unsigned int>(m_nJets); ++ii_jet){
	float dPhi = TVector2::Phi_mpi_pi(m_jet_phi->at(ii_jet) - track->phi());
	float dEta = m_jet_eta->at(ii_jet) - track->eta();
	float dR = std::sqrt(dPhi*dPhi + dEta*dEta);
	if(dR < min_jet20_dR) min_jet20_dR = dR;
	if(m_jet_pt->at(ii_jet) > 50.){
	  if(dR < min_jet50_dR) min_jet50_dR = dR;
	}
      }
      m_track_dRJet20->push_back(min_jet20_dR);
      m_track_dRJet50->push_back(min_jet50_dR);

      // IP
      m_track_d0->push_back(track->d0());
      m_track_d0Sig->push_back(xAOD::TrackingHelpers::d0significance(track));
      m_track_z0->push_back(track->z0());
      float z0PV = track->z0() - m_pv_z + track->vz();
      float z0PVSinTheta = z0PV * std::sin(track->theta());
      m_track_z0PV->push_back(z0PV);
      m_track_z0PVSinTheta->push_back(z0PVSinTheta);
      m_track_chi2Prob->push_back(TMath::Prob(track->chiSquared(), track->numberDoF()));

      // truth link
      bool has_link = false;
      if(!m_is_data){
	ElementLink<xAOD::TruthParticleContainer>truthLink;
	if(track->isAvailable<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink")){
	  truthLink = track->auxdata<ElementLink<xAOD::TruthParticleContainer>>("truthParticleLink");
	  if(truthLink.isValid()){
	    has_link = true;
	    m_track_truth_phi->push_back((*truthLink)->phi());
	    m_track_truth_eta->push_back((*truthLink)->eta());
	    m_track_truth_pt->push_back((*truthLink)->pt() * unit_GeV);
	    m_track_truth_e->push_back((*truthLink)->e() * unit_GeV);
	    m_track_truth_charge->push_back((*truthLink)->charge());
	    m_track_truth_pdgid->push_back((*truthLink)->pdgId());
	    m_track_truth_barcode->push_back((*truthLink)->barcode());
	    m_track_truth_nChildren->push_back((*truthLink)->nChildren());
	    decayR = (*truthLink)->decayVtx() != nullptr? (*truthLink)->decayVtx()->perp() : -999.;
	    auto bFactor_t = (*truthLink)->pt() / (*truthLink)->m();
	    proper_time = decayR / bFactor_t / c;
	    m_track_truth_properTime->push_back(proper_time);
	    m_track_truth_decayr->push_back(decayR);
	    m_track_truth_matchingProb->push_back(track->auxdata<float>("truthMatchProbability"));
	    if(std::fabs((*truthLink)->pdgId()) == 1000024) std::cout<<"(*truthLink)->pdgId() = "<<(*truthLink)->pdgId()<<std::endl;

	    if((*truthLink)->parent() != nullptr) {
	      m_track_truth_parent_phi->push_back((*truthLink)->parent(0)->phi());
	      m_track_truth_parent_eta->push_back((*truthLink)->parent(0)->eta());
	      m_track_truth_parent_pt->push_back((*truthLink)->parent(0)->pt() * unit_GeV);
	      m_track_truth_parent_e->push_back((*truthLink)->parent(0)->e() * unit_GeV);
	      m_track_truth_parent_charge->push_back((*truthLink)->parent(0)->charge());
	      m_track_truth_parent_pdgid->push_back((*truthLink)->parent(0)->pdgId());
	      m_track_truth_parent_barcode->push_back((*truthLink)->parent(0)->barcode());
	      m_track_truth_parent_nChildren->push_back((*truthLink)->parent(0)->nChildren());
	      decayR = (*truthLink)->parent(0)->decayVtx() != nullptr? (*truthLink)->parent(0)->decayVtx()->perp() : -999.;
	      m_track_truth_parent_decayr->push_back(decayR);
	    } else {
	      m_track_truth_parent_phi->push_back(-999.);
	      m_track_truth_parent_eta->push_back(-999.);
	      m_track_truth_parent_pt->push_back(-999.);
	      m_track_truth_parent_e->push_back(-999.);
	      m_track_truth_parent_charge->push_back(-999.);
	      m_track_truth_parent_pdgid->push_back(-999.);
	      m_track_truth_parent_barcode->push_back(-999.);
	      m_track_truth_parent_nChildren->push_back(-999.);
	      m_track_truth_parent_decayr->push_back(-999.);
	    }
	  }
	}
      }
      if(!has_link){
	m_track_truth_phi->push_back(-999.);
	m_track_truth_eta->push_back(-999.);
	m_track_truth_pt->push_back(-999.);
	m_track_truth_e->push_back(-999.);
	m_track_truth_charge->push_back(-999);
	m_track_truth_pdgid->push_back(-999);
	m_track_truth_barcode->push_back(-999);
	m_track_truth_nChildren->push_back(-999);
	m_track_truth_parent_barcode->push_back(-999);
	m_track_truth_properTime->push_back(-999.);
	m_track_truth_decayr->push_back(-999.);
	m_track_truth_matchingProb->push_back(-999.);
	m_track_truth_parent_phi->push_back(-999.);
	m_track_truth_parent_eta->push_back(-999.);
	m_track_truth_parent_pt->push_back(-999.);
	m_track_truth_parent_e->push_back(-999.);
	m_track_truth_parent_charge->push_back(-999);
	m_track_truth_parent_pdgid->push_back(-999);
	m_track_truth_parent_barcode->push_back(-999);
	m_track_truth_parent_nChildren->push_back(-999);
	m_track_truth_parent_decayr->push_back(-999.);
      }


    }
  } else {
    ATH_MSG_INFO("cannot get the track container");
    return false;
  }

  m_nTracks += static_cast<int>(nTracks);
  return true;
}
